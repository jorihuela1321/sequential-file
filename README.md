# README #

Este proyecto tiene como finalidad la comparación entre dos técnicas de indexación de archivos: Sequential File y B+Tree.
El repositorio presente contiene la implementación del Sequential File. Para revisar la implementación del B+Tree dirigirse
al repositorio en [github](https://github.com/jeffreyorihuela/b-tree-on-disk/). Se espera obtener
resultados que demuestren la superioridad del B+Tree unclustered.

### Dominio de datos ###

* El data set con el que se trabaja fue información básica acerca de computadoras en un archivo
  csv de 289.64 KB
* Cada registro esta conformado por los siguientes campos: ID(int), PRICE(int), SPEED(int), HD(int), RAM(int), SCREEN(int),
  CD(string), MULTI(string), PREMIUM(string), ADS(int), TREND(int)
* Descargar [dataset](https://www.kaggle.com/kingburrito666/basic-computer-data-set/download)

### Configuración ###

* El proyecto esta hecho en c++ 14 y de uso preferente para CLion en Linux
* CLion nos ayuda a poder correr el archivo CMakeLists.txt, se necesita seleccionar el CMakeLists.txt como un proyecto cmake
* Antes de ejecutar el proyecto descargar el dataset y ubicarlo en la siguiente dirección: sequential-file/cmake-build-debug/archive/Computers.csv
* Para el testing se usa la librería [googletest](https://github.com/google/googletest), la cual se descarga automaticamente gracias a CMake.
* Ejecutar el archivo main para ver la funcionalidad de los tests.

### Descripción de Sequential File ###

La presente técnica de indexación de archivos presenta dos clases: la principal y la auxiliar. 
La clase principal se encarga de mantener los archivos ordenados de acuerdo al key, en nuestro caso es el ID de las computadoras.
La clase auxiliar es responsable de conectar los nuevos archivos con los previos de manera que evita una alteración de los archivos en el índice principal usando un
índice aparte. Sin 
embargo tiene un límite de archivos del cuál una vez llegado a ese límite se reconstruye el índice principal haciendo un ordenamiento.

1. INSERT
   
    Este método en una primera instancia, ubica el registro previo al lugar de la inserción a través del método SEARCH.
Una vez encontrado el registro previo, modificamos para que este registro apunte hacia el nuevo registro.
   En caso de que el nuevo registro puede almacenarse en el archivo auxiliar lo ubicamos ahí. Existe la posibilidad que 
   el archivo auxiliar este lleno y no se pueda almacenar ningún registro más. Para esto procedemos a 
   reconstruir el índice principal ordenamente incluyendo a los archivos del índice auxiliar.
   

2. SEARCH
  
    El método aprovecha el orden de los archivos en el índice principal usando
    una búsqueda binaria. Si no se llega a encontrar el registro,
   entonces devuelve el registro previo según el orden del key. Para dicho registro nos encargamos
   de verificar si su siguiente apunta al índice auxiliar o no, en caso no apunte al auxiliar
   retornamos este registo. Si el siguiente se encuentra en el archivo auxiliar, haremos una busqueda
   de los archivos enlazados en el auxiliar.


3. SEARCH BY RANGE

    Este método usa la búsqueda directa del anterior. Se consigue el primero y desde esa forma
    se logra recorrer los archivos enlazados hasta encontrar el último registro. Mientras se procede
    a hacer este paso, se almacenan los registros cargados a memoria principal en un contenedor, el
    cuál sera retornado.


Es importante resaltar que los métodos usados pueden llegar a verse comprometidos cuando se trate de un volumen
más grande de datos, ya que el método INSERT, carga en memoria principal todos los archivos del índice principal y secundario
para luego poder hacer un ordenamiento. Este problema puede ser resuelto a través de un MERGE de índices principales cuyo tamaño cuando sea cargado a memoria
principal no afecte, estos bloques principales pueden llegar a tener sus propios auxiliares. Finalmente cada bloque debe
estar ordenado y tener una metada donde almacene la información del siguiente bloque. A este approach podríamos llamarlo un 
Multi Sequential File.
### Resultados Experimentales ###

Estos resultados fueron medidos desde una computadora i5 10400f, 16 Gb Ram, 512 Gb SSD, 1 Tb HDD y OS: Ubuntu 21.04 

1. Sequential File
  
    Insertar todos los archivos: 17143ms
    
    Búsqueda por Rango: 13 ms
  

2. B+Tree Index
    
    Insertar todos los archivos: 59ms
    
    Búsqueda por Rango: 12 ms

### Discusión de los resultados ###

De acuerdo a los resultados obtenidos usando el mismo data set y las mismas consultas podemos observar
que respecto al procedimiento de inserción el índice B+Tree es muy superior al Sequential File. Debido
a que el árbol no necesita hacer un ordenamiento ni tampoco tiene dependencia de un archivo auxiliar, al
contrario el método de inserción del B+Tree garantiza que la estructura se mantenga balanceada.
Respecto a la búsqueda por Rango el B+Tree sigue siendo más óptimo, este resultado se puede explicar debido
a que no necesita hacer muchas lecturas a disco ya que un nodo en el árbol representa una ṕagina y la búsqueda
por rango es a nivel de las hojas. Estas hojas estan enlazadas reduciendo el número de lecturas a disco.

### Link del video

https://drive.google.com/file/d/1LCbsHTXh72CeIYihVkdBXrazG9rVO-iX/view?usp=sharing