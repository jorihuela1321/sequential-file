//
// Created by jaoks on 5/24/21.
//

#include "Record.cpp"
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class AuxFile: public fstream {
private:
    string fileName;
public:
    AuxFile(string _fileName,bool trunc = false) {
        this->fileName = _fileName;
        if(!good() || trunc) {
            open(fileName.data(), fstream::in | fstream::out | fstream::trunc | fstream::binary);
        } else {
            open(fileName.data(), fstream::in | fstream::binary);
        }
    }

    int size() {
        int numRecords = 0;
        if(is_open()) {
            seekg(0, ios::end);
            long bytes = tellg();
            numRecords = bytes/sizeof(Registro);
        } else {
            cout << "Could not open the file\n";
        }
        return numRecords;
    }

    bool add(Registro _register_) {
        clear();
        int _size_ = size();
        if(_size_ == 4) return false;
        seekg(_size_*sizeof(Registro));
        _register_.pos = _size_;
        _register_.data = false;
        write((char*) &_register_, sizeof(_register_));
        return true;
    };

    void update(Registro _register_) {
        clear();
        seekg(_register_.pos*sizeof(Registro));
        write((char*) &_register_, sizeof(_register_));
    }

    Registro search(Registro prev, const int key) {
        int total_size = size();
        Registro tmp;
        for (int i = 0; i < total_size; ++i) {
            seekg(i*sizeof(Registro));
            read((char*)&tmp, sizeof(Registro));
            if(tmp < key) {
                prev = tmp;
            } else if (tmp > key) {
                continue;
            } else {
                return tmp;
            }
        }
        return prev;
    }
    vector<Registro> getAll() {
        vector<Registro> records;
        clear();
        seekp(0, ios::beg);
        Registro registro;
        while(read((char*)&registro, sizeof(registro))) {
            records.push_back(registro);
        }
        //DELETE AUX FILE
        close();
        open(fileName.data(), fstream::in | fstream::out | fstream::trunc | fstream::binary);
        return records;
    }

    ~AuxFile() {
        close();
    }
};