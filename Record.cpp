//
// Created by jaoks on 5/23/21.
//

struct Registro {
    int id;
    int price;
    int speed;
    int hd;
    int ram;
    int screen;
    char cd[4];
    char multi[4];
    char premium[4];
    int ads;
    int trend;
    int next = -1;
    bool aux = false; //IF TRUE THEN NEXT IS IN AUX FILE
    int pos = -1; //To update faster a record
    bool data = true; //IF TRUE THEN RECORD IS IN DATA FILE

    bool operator<(const int id) const {
        return this->id < id;
    }

    bool operator>(const int id) const {
        return this->id > id;
    }

    bool operator<(const Registro registro) const {
        return this->id < registro.id;
    }
};

struct less_than_key {
    inline bool operator() (const Registro &rec1, const Registro &rec2) {
        return rec1 < rec2;
    }
};