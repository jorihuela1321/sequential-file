//
// Created by jaoks on 5/24/21.
//

#include "AuxiliarFile.cpp"
#include <algorithm>

class SequentialFile : protected fstream{
private:
    string fileName;
    AuxFile* auxFile;
public:
    SequentialFile(string _fileName, bool trunc = false) {
        this->fileName = _fileName;
        if(!good() || trunc) {
            open(fileName.data(), fstream::in | fstream::out | fstream::trunc | fstream::binary);
        } else {
            open(fileName.data(), fstream::in | fstream::binary);
        }
        auxFile = new AuxFile("aux.bin", trunc);
    }

    void insertAll(vector<Registro> records) {
        sort(records.begin(), records.end(), less_than_key());
        if (is_open()) {
            int i;
            for (i = 0; i < records.size()-1; i++) {
                clear();
                records[i].next = i+1;
                Registro record = records[i];
                seekg(i*sizeof(Registro));
                record.pos = i;
                record.data = true;
                record.aux = false;
                write((char*) &record, sizeof(record));
                cout << record.id << ' ' << record.next << endl;
            }
            Registro record = records[i];
            seekg(i*sizeof(Registro));
            record.pos = i;
            write((char*) &record, sizeof(record));
            cout << record.id << ' ' << record.next << endl;
        } else {
            cout << "Could not open the file\n";
        }
        cout << endl;
    }

    int size() {
        int numRecords = 0;
        if(is_open()) {
            clear();
            seekg(0, ios::end);
            long bytes = tellg();
            numRecords = bytes/sizeof(Registro);
        } else {
            cout << "Could not open the file\n";
        }
        return numRecords;
    }

    Registro search(const int key) {
        int left = 0;
        int right = size() - 1;
        Registro record_2;
        while (right >= left) {
            int mid = (left+right)/2;
            seekg(mid*sizeof(Registro));
            Registro record;
            read((char*)&record, sizeof(Registro));
            if (record < key ) {
                left = mid + 1;
                record_2 = record;
            } else if (record > key) {
                right = mid - 1;
            } else {
                return record;
            }
        }

        if(record_2.aux) {
            return auxFile->search(record_2, key);
        } else return record_2;
    }

    void add(Registro n_record) {
        Registro prev_record = search(n_record.id);
        int pos_in_aux = auxFile->size();
        if(pos_in_aux < 4) {
            n_record.next = prev_record.next;
            n_record.aux = prev_record.aux;
            prev_record.next = pos_in_aux;
            prev_record.aux = true;
            auxFile->add(n_record);
            if(!prev_record.data) {
                auxFile->update(prev_record);
            } else {
                clear();
                seekg(prev_record.pos*sizeof(Registro));
                write((char*) &prev_record, sizeof(Registro));
            }
        } else {
            //REORDENAR FILE
            cout << "REORDENAR FILE " << endl;
            vector<Registro> aux = auxFile->getAll();

            clear();
            seekg(0, ios::beg);
            Registro tmp;
            while(read((char*)&tmp, sizeof(tmp))) {
                aux.push_back(tmp);
            }
            aux.push_back(n_record);
            this->insertAll(aux);
        }
    }

    bool isSameKey(const char* record1, const char* record2) {
        int i = 0;
        while(record1[i] != '\0' && record2[i] != '\0') {
            if(record1[i] != record2[i]) return false;
            i++;
        }
        return true;
    }

    vector<Registro> search(const int begin, const int end) {
        Registro first = search(begin);
        vector<Registro> records;
        records.push_back(first);
        Registro tmp = first;
        while( tmp.id != end) {
            if(tmp.aux) {
                auxFile->clear();
                auxFile->seekg(tmp.next * sizeof(Registro));
                auxFile->read((char*) &tmp, sizeof(Registro));
            } else {
                clear();
                seekg(tmp.next*sizeof(Registro));
                read((char*) &tmp, sizeof(Registro));
            }
            records.push_back(tmp);
        }
        return records;
    }

    ~SequentialFile() {
        close();
    }
};
