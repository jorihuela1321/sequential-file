//
// Created by jaoks on 5/23/21.
//

#include "gtest/gtest.h"
#include "../SequentialFile.cpp"
#include <iostream>

using namespace std;

struct SequentialFileTest : public ::testing::Test {

};

TEST_F(SequentialFileTest, Test1) {
    cout << "TEST 1 : INSERT" << endl;
    fstream in;
    in.open("archive/Computers.csv", ios::in);
    if(!in.is_open()) {
        cout << "is not opened" << endl;
        FAIL();
    }

    SequentialFile sequentialFile("computers.bin", true);

    string line;
    getline(in, line);
    Registro record;
    while(getline(in, line)) {
        int pos_in_line = 0;
        vector<string> tokens;
        string token;
        while((pos_in_line=line.find(',')) != string::npos) {
            token = line.substr(0, pos_in_line);
            line.erase(0, pos_in_line+1);
            tokens.push_back(token);
        }
        tokens[0].erase(remove(tokens[0].begin(), tokens[0].end(), '\"'), tokens[0].end());
        record.id = stoi(tokens[0]);
        record.price = stoi(tokens[1]);
        record.speed = stoi(tokens[2]);
        record.hd = stoi(tokens[3]);
        record.ram = stoi(tokens[4]);
        record.screen = stoi(tokens[5]);
        tokens[6].erase(remove(tokens[6].begin(), tokens[6].end(), '\"'), tokens[6].end());
        strcpy(record.cd, tokens[6].c_str());
        tokens[7].erase(remove(tokens[7].begin(), tokens[7].end(), '\"'), tokens[7].end());
        strcpy(record.multi, tokens[7].c_str());
        tokens[8].erase(remove(tokens[8].begin(), tokens[8].end(), '\"'), tokens[8].end());
        strcpy(record.premium, tokens[8].c_str());
        record.ads = stoi(tokens[9]);
        record.trend = stoi(token);
        sequentialFile.add(record);
    }
    in.close();
}

TEST_F(SequentialFileTest, Test2) {
    cout << "TEST 2: SEARCH" << endl;
    SequentialFile sequentialFile("computers.bin", false);
    Registro record = sequentialFile.search(1420);
    cout << "ID: "<< record.id << endl;
    cout << "PRICE: "<< record.price << endl;
    cout << "SPEED: " << record.speed << endl;
    cout << "HD: " << record.hd << endl;
    cout << "RAM: " << record.ram << endl;
}

TEST_F(SequentialFileTest, Test3) {
    cout << "TEST 3: SEARCH BY RANGE" << endl;
    SequentialFile sequentialFile("computers.bin", false);
    vector<Registro> results = sequentialFile.search(394, 5120);

    for (int i = 0; i < results.size(); ++i) {
        Registro record = results[i];
        cout << "ID: "<< record.id << " - ";
        cout << "PRICE: "<< record.price << " - ";
        cout << "SPEED: " << record.speed << " - ";
        cout << "HD: " << record.hd << " - ";
        cout << "RAM: " << record.ram << endl;
    }
}